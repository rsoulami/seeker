import React, { Component } from "react";
import { Admin, Resource } from "react-admin";

// Icons
import Person from "@material-ui/icons/Person";

// Configs
import authProvider from "./authProvider";
import dataProvider from "./dataProvider";

// Custom Routes
import Routes from "./routes";

// Custom login page
import Login from "./Login";

// Ressources
import {
  ProfilesList,
  ProfileEdit,
  ProfileCreate
} from "./ressources/profiles";

class App extends Component {
  render() {
    return (
      <Admin
        dataProvider={dataProvider}
        authProvider={authProvider}
        customRoutes={Routes}
        loginPage={Login}
      >
        <Resource
          name="profiles"
          list={ProfilesList}
          edit={ProfileEdit}
          create={ProfileCreate}
          icon={Person}
        />
      </Admin>
    );
  }
}

export default App;
