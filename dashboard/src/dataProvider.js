import {
  GET_LIST,
  GET_ONE,
  GET_MANY,
  CREATE,
  UPDATE,
  fetchUtils
} from "react-admin";
import { stringify } from "query-string";

const API_URL = "http://localhost:4000/api/v1";
const REGISTER = "REGISTER";

/**
 * @desc Replace "_id" with "id"
 * @param {*} item
 */
const fixId = item => {
  item.id = item._id;
  delete item._id;

  return item;
};

/**
 * @desc Validate a JSON string
 * @param {*} json
 */
const validJSON = json => {
  try {
    JSON.parse(json);
    return true;
  } catch (e) {
    return false;
  }
};

/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
const convertDataProviderRequestToHTTP = (type, resource, params) => {
  switch (type) {
    case GET_LIST: {
      // const { page, perPage } = params.pagination;
      // const { field, order } = params.sort;
      // const query = {
      //   sort: JSON.stringify([field, order]),
      //   range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
      //   filter: JSON.stringify(params.filter)
      // };
      return { url: `${API_URL}/${resource}`, options: {} };
    }
    case GET_ONE: {
      return { url: `${API_URL}/${resource}/${params.id}`, options: {} };
    }
    case GET_MANY: {
      const query = {
        filter: JSON.stringify({ id: params.ids })
      };
      return { url: `${API_URL}/${resource}?${stringify(query)}`, options: {} };
    }
    case UPDATE: {
      return {
        url: `${API_URL}/${resource}/${params.id}`,
        options: { method: "PUT", body: JSON.stringify(params.data) }
      };
    }
    case CREATE: {
      return {
        url: `${API_URL}/${resource}`,
        options: { method: "POST", body: JSON.stringify(params.data) }
      };
    }
    case REGISTER: {
      return {
        url: `${API_URL}/${resource}`,
        options: { method: "POST", body: JSON.stringify(params.data) }
      };
    }
    default: {
      throw new Error(`Unsupported fetch action type ${type}`);
    }
  }
};

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} Data Provider response
 */
const convertHTTPResponseToDataProvider = (
  response,
  type,
  resource,
  params
) => {
  const { headers, json } = response;

  switch (type) {
    case GET_LIST: {
      return {
        data: json.length ? json.map(fixId) : [],
        total: parseInt(headers.get("x-total-count"), 10)
      };
    }
    case GET_ONE: {
      const id = json._id;
      delete json._id;
      return { data: { ...params.data, id, ...json } };
    }
    case CREATE: {
      return { data: { ...params.data, id: json._id } };
    }
    case UPDATE: {
      const id = json._id;
      delete json._id;
      return { data: { ...params.data, id, ...json } };
    }
    case REGISTER: {
      return { data: { ...params.data, id: json._id } };
    }
    default: {
      return { data: json };
    }
  }
};

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for response
 */
export default (type, resource, params) => {
  const { url, options } = convertDataProviderRequestToHTTP(
    type,
    resource,
    params
  );

  if (!options.headers) {
    options.headers = new Headers({ Accept: "application/json" });
  }

  // Custom headers
  options.headers.set("Authorization", "Bearer " + window.localStorage.token);

  return fetchUtils
    .fetchJson(url, options)
    .then(response =>
      convertHTTPResponseToDataProvider(response, type, resource, params)
    )
    .catch(err => {
      console.dir(err);
    });
};
