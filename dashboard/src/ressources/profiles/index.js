import React from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  TextField,
  EmailField,
  EditButton,
  SimpleForm,
  TextInput
} from "react-admin";

import ProfileTitle from "./Title";
import ProfileFilter from "./Filter";

// mobility: [""];

// seniority: "";
// technology: [""];
// tools: ["Python", "SQL", "Spark"];

export const ProfilesList = props => (
  <List filters={<ProfileFilter />} {...props}>
    <Datagrid rowClick="edit">
      <TextField source="name" />
      <EmailField source="mail" />
      <TextField source="phone" />
      <TextField source="seniority" />
      <TextField source="contract" />
      <TextField source="experience" />
      <TextField source="disponibility" />
      <TextField source="description" />
      <EditButton />
    </Datagrid>
  </List>
);

export const ProfileEdit = props => (
  <Edit title={<ProfileTitle />} {...props}>
    <SimpleForm>
      <TextInput source="id" />
      <TextInput source="name" />
      <TextInput source="username" />
      <TextInput source="email" />
      <TextInput source="address.street" />
      <TextInput source="phone" />
      <TextInput source="website" />
      <TextInput source="company.name" />
    </SimpleForm>
  </Edit>
);

export const ProfileCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="mail" />
      <TextInput source="phone" />
      <TextInput source="seniority" />
      <TextInput source="contract" />
      <TextInput source="experience" />
      <TextInput source="disponibility" />
      <TextInput source="description" />
    </SimpleForm>
  </Create>
);
