import React from "react";

const ProfileTitle = ({ record }) => (
  <span>{record ? `${record.name}` : ""}</span>
);

export default ProfileTitle;
