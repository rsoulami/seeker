import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { propTypes, reduxForm, Field } from "redux-form";
import { connect } from "react-redux";
import compose from "recompose/compose";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";
import LockIcon from "@material-ui/icons/LockOutline";

import { Notification, translate, userLogin, CREATE } from "react-admin";
import { Link } from "ra-ui-materialui";

import dataProvider from "./dataProvider";

const styles = theme => ({
  main: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    alignItems: "center",
    justifyContent: "flex-start",
    background: "#eee",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  card: {
    minWidth: 500,
    maxWidth: "600px",
    marginTop: "6em"
  },
  avatar: {
    margin: "1em",
    display: "flex",
    justifyContent: "center"
  },
  icon: {
    backgroundColor: theme.palette.secondary.main
  },
  hint: {
    marginTop: "1em",
    display: "flex",
    justifyContent: "center",
    color: theme.palette.grey[500]
  },
  form: {
    padding: "0 1em 1em 1em"
  },
  input: {
    marginTop: "1em"
  },
  actions: {
    padding: "0 1em 1em 1em",
    margin: "60px 0 20px"
  },
  links: {
    padding: "1em",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    fontSize: "14px",
    textDecoration: "underline"
  }
});

// see http://redux-form.com/6.4.3/examples/material-ui/
const renderInput = ({
  meta: { touched, error } = {},
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    error={!!(touched && error)}
    helperText={touched && error}
    {...inputProps}
    {...props}
    fullWidth
  />
);

class Register extends Component {
  state = {
    open: false,
    tile: "",
    message: "",
    redirect: false
  };

  register = auth => {
    dataProvider("REGISTER", "access/register", {
      data: auth
    }).then(response => {
      if (typeof response !== undefined) {
        this.setState({
          open: true,
          title: "Welcome",
          message: "You are a registerd user now",
          redirect: true
        });
      } else {
        this.setState({
          open: true,
          title: "Oops!",
          message: "Something wrong happend",
          redirect: false
        });
      }
    });
  };

  closeDialog = () => {
    const { history } = this.props;
    this.setState({ open: false }, () => {
      if (this.state.redirect) {
        history.push("/login");
      }
    });
  };

  render() {
    const { classes, handleSubmit, isLoading, translate } = this.props;
    return (
      <div className={classes.main}>
        <Dialog
          open={this.state.open}
          onClose={this.closeDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{this.state.title}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
        <Card className={classes.card}>
          <div className={classes.avatar}>
            <Avatar className={classes.icon}>
              <LockIcon />
            </Avatar>
          </div>
          <form onSubmit={handleSubmit(this.register)}>
            <div className={classes.hint}>Inscrivez vous !</div>
            <div className={classes.form}>
              <Grid container spacing={24}>
                <Grid item xs={6}>
                  <div className={classes.input}>
                    <Field
                      name="fullname"
                      component={renderInput}
                      label="Nom complet"
                      disabled={isLoading}
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.input}>
                    <Field
                      name="username"
                      component={renderInput}
                      label="Pseudo"
                      disabled={isLoading}
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className={classes.input}>
                    <Field
                      name="email"
                      component={renderInput}
                      label="Email"
                      disabled={isLoading}
                      fullWidth
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.input}>
                    <Field
                      name="password"
                      component={renderInput}
                      label="Mot de passe"
                      type="password"
                      disabled={isLoading}
                      fullWidth
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.input}>
                    <Field
                      name="confirm_password"
                      component={renderInput}
                      label="Confirmer votre mot de passe"
                      type="password"
                      disabled={isLoading}
                      fullWidth
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
            <CardActions className={classes.actions}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <Button
                    variant="raised"
                    type="submit"
                    color="primary"
                    disabled={isLoading}
                    className={classes.button}
                    fullWidth
                  >
                    {isLoading && <CircularProgress size={25} thickness={2} />}
                    Créer votre compte
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <Button
                    variant="outlined"
                    color="primary"
                    component={Link}
                    to="login"
                    className={classes.button}
                    fullWidth
                  >
                    Login
                  </Button>
                </Grid>
              </Grid>
            </CardActions>
            {/* <div className={classes.links}>
                            <Link to="/login">Login</Link>
                        </div> */}
          </form>
        </Card>
        <Notification />
      </div>
    );
  }
}

Register.propTypes = {
  ...propTypes,
  authProvider: PropTypes.func,
  classes: PropTypes.object,
  previousRoute: PropTypes.string,
  translate: PropTypes.func.isRequired,
  userLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({ isLoading: state.admin.loading > 0 });

const enhance = compose(
  translate,
  reduxForm({
    form: "register",
    validate: (values, props) => {
      const errors = {};
      const { translate } = props;
      if (!values.fullname) {
        errors.fullname = translate("ra.validation.required");
      }
      if (!values.username) {
        errors.username = translate("ra.validation.required");
      }
      if (!values.email) {
        errors.email = translate("ra.validation.required");
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        errors.email = translate("ra.validation.invalid_email");
      }
      if (!values.role) {
        errors.role = translate("ra.validation.required");
      }
      if (!values.password) {
        errors.password = translate("ra.validation.required");
      }
      if (!values.password) {
        errors.confirm_password = translate("ra.validation.required");
      }
      if (!values.confirm_password) {
        errors.confirm_password = translate("ra.validation.required");
      } else if (values.confirm_password !== values.password) {
        errors.confirm_password = translate("ra.validation.pwd_mismatch");
      }

      return errors;
    }
  }),
  connect(
    mapStateToProps,
    { userLogin }
  ),
  withStyles(styles)
);

export default enhance(Register);
