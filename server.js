const express = require("express");

// Loading config & utils
const esSync = require("./utils/essync");
const db = require("./config/db.cfg")();
const Profile = require("./models/Profile");

// Loading controllers
const { access, profiles } = require("./routes/index");

const middleware = require("./middleware/index");
const { PORT } = require("./config/net.cfg");

const server = express();

// Configure middleware
middleware(server);

//DB synchronisation
esSync(Profile);

db.on("open", () => console.log("DB Connected"));
db.on("error", () => console.log("Error connecting to database"));

//Server APIs
server.use("/api/v1/access", access);
server.use("/api/v1/profiles", profiles);
server.use("/api/v1", (req, res) =>
  res.json({ msg: "Welcome to seeker API v1" })
);
server.use("/", (req, res) => res.redirect("/api/v1"));

server.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
