const profile = require("express").Router();
const passport = require("passport");

// ORM profile model
const Profile = require("../models/Profile");

// Elasticsearch Client
const esClient = require("../config/es.cfg");

// passport.authenticate("jwt", { session: false }),

/**
 * @route GET /profiles
 * @desc Get user profiles list
 */
profile.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const query = {
      // bool: {
      //   must: [
      //     {
      //       multi_match: {
      //         query: term,
      //         fields: ["job", "name", "tools", "technology", "seniority"]
      //       }
      //     },
      //     ...(term && [{ match: { term: term } }]),
      //     ...(tech && [{ match: { technology: tech } }]),
      //     ...(tool && [{ match: { tools: tool } }]),
      //     ...(sen && [{ match: { seniority: sen } }])
      //   ]
      // }
      match_all: {}
    };

    esClient
      .search({
        index: "profiles",
        body: {
          size: 10,
          from: 0,
          query
        }
      })
      .then(results => {
        res.json(
          results.hits.hits.map(item => {
            const record = {
              _id: item._id,
              ...item._source
            };
            return record;
          })
        );
      })
      .catch(console.error);
  }
);

/**
 * @route GET /profiles/:id
 * @desc Get profile
 */
profile.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { id } = req.params;

    Profile.findById(id, (err, p) => {
      if (err) {
        res.sendStatus(500);
      }

      res.json(p);
    });
  }
);

/**
 * @route POST /profiles
 * @desc Add profile
 */
profile.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    console.log(req.body);
    const {
      name,
      age,
      phone,
      mail,
      network,
      experience,
      seniority,
      job,
      tools,
      technology,
      disponibility,
      city,
      mobility,
      contract,
      secteur,
      salary,
      description
    } = req.body;

    const newProfile = new Profile({
      name: name,
      age: age,
      phone: phone,
      mail: mail,
      network: network,
      experience: experience,
      seniority: seniority,
      job: job,
      tools: tools && tools.split(","),
      technology: technology && technology.split(","),
      disponibility: disponibility,
      city: city,
      mobility: mobility && mobility.split(","),
      contract: contract,
      secteur: secteur,
      salary: salary,
      description: description
    });

    newProfile.save(err => {
      if (err) {
        res.sendStatus(500);
      }

      res.statusMessage = "Profile added succesfully";
      res.status(201).json(newProfile);
    });
  }
);

/**
 * @route PUT /profiles/:id
 * @desc Update profile
 */
profile.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const query = { _id: req.params.id };

    const update = ({
      name,
      age,
      phone,
      mail,
      network,
      experience,
      seniority,
      job,
      tools,
      technology,
      disponibility,
      city,
      mobility,
      contract,
      secteur,
      salary,
      description
    } = req.body);

    const options = { multi: false };

    Profile.updateOne(query, update, err => {
      if (err) {
        res.sendStatus(500);
      }

      res.statusMessage = "Profile updated succesfully";
      res.sendStatus(201);
    });
  }
);

module.exports = profile;
