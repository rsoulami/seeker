const accessRouter = require("./access");
const profileRouter = require("./profile");

// Exporting routing modules
module.exports = {
  access: accessRouter,
  profiles: profileRouter
};
