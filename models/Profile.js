const Schema = require('mongoose').Schema;
const mongoosastic = require('mongoosastic');
const db = require('../config/db.cfg')();

const ProfileSchema = new Schema({
    name: { type: String, required: true, default: '' },
    age: { type: String, required: false, default: true },
    phone: { type: String, required: false, default: true },
    mail: { type: String, required: false, default: true },
    network: { type: String, required: false, default: true },
    experience: { type: String, required: false, default: true },
    seniority: { type: String, required: false, default: true },
    job: { type: String, required: false, default: true },
    tools: { type: Array, required: false, default: true },
    technology: { type: Array, required: false, default: true },
    disponibility: { type: String, required: false, default: true },
    city: { type: String, required: false, default: true },
    mobility: { type: Array, required: false, default: true },
    contract: { type: String, required: false, default: true },
    secteur: { type: String, required: false, default: true },
    salary: { type: String, required: false, default: true },
    description: { type: String, required: false, default: true }
},{
    collection: 'subscriptions'
});

ProfileSchema.plugin(mongoosastic);

module.exports =  db.model('Profile', ProfileSchema);
